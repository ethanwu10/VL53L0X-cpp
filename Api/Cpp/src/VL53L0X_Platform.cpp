#include "VL53L0X_Platform.hpp"

using namespace VL53L0X;
VL53L0X_Error VL53L0X_Platform::UpdateByte(uint8_t index, uint8_t AndData, uint8_t OrData)
{
	VL53L0X_Error status;
	uint8_t data;
	status = RdByte(index, &data);
	if (status != VL53L0X_ERROR_NONE)
	{
		return status;
	}
	data = (data & AndData) | OrData;
	status = WrByte(index, data);
	return status;
}
