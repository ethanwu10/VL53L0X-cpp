#include "VL53L0X.hpp"
#include "VL53L0X_Platform.hpp"
#include "vl53l0x_platform.h"
#include "vl53l0x_api.h"

namespace VL53L0X
{

class VL53L0X::Impl
{
public:

	Impl(VL53L0X_Platform* Platform);

	virtual ~Impl();

	VL53L0X_Dev_t Dev;
	VL53L0X_Dev_t* pDev;
};

VL53L0X::Impl::Impl(VL53L0X_Platform* Platform) : pDev(&Dev)
{
	Dev.ImplData = Platform;
	memset(&Dev.Data, 0xff, sizeof(Dev.Data)); // Init memory for ST lib
}

VL53L0X::Impl::~Impl()
{}

VL53L0X::VL53L0X(VL53L0X_Platform* Platform) : impl(new Impl(Platform))
{}

VL53L0X::~VL53L0X()
{
	delete impl;
}


::VL53L0X::VL53L0X_Error VL53L0X::GetProductRevision(uint8_t* pProductRevisionMajor, uint8_t* pProductRevisionMinor)
{
	return (VL53L0X_Error)VL53L0X_GetProductRevision(impl->pDev, pProductRevisionMajor, pProductRevisionMinor);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetDeviceInfo(VL53L0X_DeviceInfo_t* pVL53L0X_DeviceInfo)
{
	return (VL53L0X_Error)VL53L0X_GetDeviceInfo(impl->pDev, (::VL53L0X_DeviceInfo_t*) pVL53L0X_DeviceInfo);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetDeviceErrorStatus(VL53L0X_DeviceError* pDeviceErrorStatus)
{
	return (VL53L0X_Error)VL53L0X_GetDeviceErrorStatus(impl->pDev, (::VL53L0X_DeviceError*) pDeviceErrorStatus);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetRangeStatusString(uint8_t RangeStatus, char* pRangeStatusString)
{
	return (VL53L0X_Error)VL53L0X_GetRangeStatusString(RangeStatus, pRangeStatusString);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetDeviceErrorString(VL53L0X_DeviceError ErrorCode, char* pDeviceErrorString)
{
	return (VL53L0X_Error)VL53L0X_GetDeviceErrorString(ErrorCode, pDeviceErrorString);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetPalErrorString(VL53L0X_Error PalErrorCode, char* pPalErrorString)
{
	return (VL53L0X_Error)VL53L0X_GetPalErrorString(PalErrorCode, pPalErrorString);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetPalStateString(VL53L0X_State PalStateCode, char* pPalStateString)
{
	return (VL53L0X_Error)VL53L0X_GetPalStateString(PalStateCode, pPalStateString);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetPalState(VL53L0X_State* pPalState)
{
	return (VL53L0X_Error)VL53L0X_GetPalState(impl->pDev, (::VL53L0X_State*) pPalState);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetPowerMode(VL53L0X_PowerModes PowerMode)
{
	return (VL53L0X_Error)VL53L0X_SetPowerMode(impl->pDev, PowerMode);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetPowerMode(VL53L0X_PowerModes* pPowerMode)
{
	return (VL53L0X_Error)VL53L0X_GetPowerMode(impl->pDev, (::VL53L0X_PowerModes*) pPowerMode);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetOffsetCalibrationDataMicroMeter(int32_t OffsetCalibrationDataMicroMeter)
{
	return (VL53L0X_Error)VL53L0X_SetOffsetCalibrationDataMicroMeter(impl->pDev, OffsetCalibrationDataMicroMeter);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetOffsetCalibrationDataMicroMeter(int32_t* pOffsetCalibrationDataMicroMeter)
{
	return (VL53L0X_Error)VL53L0X_GetOffsetCalibrationDataMicroMeter(impl->pDev, pOffsetCalibrationDataMicroMeter);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetLinearityCorrectiveGain(int16_t LinearityCorrectiveGain)
{
	return (VL53L0X_Error)VL53L0X_SetLinearityCorrectiveGain(impl->pDev, LinearityCorrectiveGain);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetLinearityCorrectiveGain(uint16_t* pLinearityCorrectiveGain)
{
	return (VL53L0X_Error)VL53L0X_GetLinearityCorrectiveGain(impl->pDev, pLinearityCorrectiveGain);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetGroupParamHold(uint8_t GroupParamHold)
{
	return (VL53L0X_Error)VL53L0X_SetGroupParamHold(impl->pDev, GroupParamHold);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetUpperLimitMilliMeter(uint16_t* pUpperLimitMilliMeter)
{
	return (VL53L0X_Error)VL53L0X_GetUpperLimitMilliMeter(impl->pDev, pUpperLimitMilliMeter);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetTotalSignalRate(FixPoint1616_t* pTotalSignalRate)
{
	return (VL53L0X_Error)VL53L0X_GetTotalSignalRate(impl->pDev, pTotalSignalRate);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetDeviceAddress(uint8_t DeviceAddress)
{
	::VL53L0X::VL53L0X_Error status = (VL53L0X_Error)VL53L0X_SetDeviceAddress(impl->pDev, DeviceAddress);
	reinterpret_cast<VL53L0X_Platform*>(impl->pDev->ImplData)->UpdateAddress(DeviceAddress);
	return status;
}

::VL53L0X::VL53L0X_Error VL53L0X::DataInit()
{
	return (VL53L0X_Error)VL53L0X_DataInit(impl->pDev);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetTuningSettingBuffer(uint8_t* pTuningSettingBuffer, uint8_t UseInternalTuningSettings)
{
	return (VL53L0X_Error)VL53L0X_SetTuningSettingBuffer(impl->pDev, pTuningSettingBuffer, UseInternalTuningSettings);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetTuningSettingBuffer(uint8_t** ppTuningSettingBuffer, uint8_t* pUseInternalTuningSettings)
{
	return (VL53L0X_Error)VL53L0X_GetTuningSettingBuffer(impl->pDev, ppTuningSettingBuffer, pUseInternalTuningSettings);
}

::VL53L0X::VL53L0X_Error VL53L0X::StaticInit()
{
	return (VL53L0X_Error)VL53L0X_StaticInit(impl->pDev);
}

::VL53L0X::VL53L0X_Error VL53L0X::WaitDeviceBooted()
{
	return (VL53L0X_Error)VL53L0X_WaitDeviceBooted(impl->pDev);
}

::VL53L0X::VL53L0X_Error VL53L0X::ResetDevice()
{
	return (VL53L0X_Error)VL53L0X_ResetDevice(impl->pDev);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetDeviceParameters(const VL53L0X_DeviceParameters_t* pDeviceParameters)
{
	return (VL53L0X_Error)VL53L0X_SetDeviceParameters(impl->pDev, (::VL53L0X_DeviceParameters_t*) pDeviceParameters);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetDeviceParameters(VL53L0X_DeviceParameters_t* pDeviceParameters)
{
	return (VL53L0X_Error)VL53L0X_GetDeviceParameters(impl->pDev, (::VL53L0X_DeviceParameters_t*) pDeviceParameters);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetDeviceMode(VL53L0X_DeviceModes DeviceMode)
{
	return (VL53L0X_Error)VL53L0X_SetDeviceMode(impl->pDev, DeviceMode);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetDeviceMode(VL53L0X_DeviceModes* pDeviceMode)
{
	return (VL53L0X_Error)VL53L0X_GetDeviceMode(impl->pDev, (::VL53L0X_DeviceModes*) pDeviceMode);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetRangeFractionEnable(uint8_t Enable)
{
	return (VL53L0X_Error)VL53L0X_SetRangeFractionEnable(impl->pDev, Enable);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetFractionEnable(uint8_t* pEnable)
{
	return (VL53L0X_Error)VL53L0X_GetFractionEnable(impl->pDev, pEnable);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetHistogramMode(VL53L0X_HistogramModes HistogramMode)
{
	return (VL53L0X_Error)VL53L0X_SetHistogramMode(impl->pDev, HistogramMode);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetHistogramMode(VL53L0X_HistogramModes* pHistogramMode)
{
	return (VL53L0X_Error)VL53L0X_GetHistogramMode(impl->pDev, (::VL53L0X_HistogramModes*) pHistogramMode);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetMeasurementTimingBudgetMicroSeconds(uint32_t MeasurementTimingBudgetMicroSeconds)
{
	return (VL53L0X_Error)VL53L0X_SetMeasurementTimingBudgetMicroSeconds(impl->pDev, MeasurementTimingBudgetMicroSeconds);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetMeasurementTimingBudgetMicroSeconds(uint32_t* pMeasurementTimingBudgetMicroSeconds)
{
	return (VL53L0X_Error)VL53L0X_GetMeasurementTimingBudgetMicroSeconds(impl->pDev, pMeasurementTimingBudgetMicroSeconds);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetVcselPulsePeriod(VL53L0X_VcselPeriod VcselPeriodType, uint8_t* pVCSELPulsePeriod)
{
	return (VL53L0X_Error)VL53L0X_GetVcselPulsePeriod(impl->pDev, VcselPeriodType, pVCSELPulsePeriod);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetVcselPulsePeriod(VL53L0X_VcselPeriod VcselPeriodType, uint8_t VCSELPulsePeriod)
{
	return (VL53L0X_Error)VL53L0X_SetVcselPulsePeriod(impl->pDev, VcselPeriodType, VCSELPulsePeriod);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetSequenceStepEnable(VL53L0X_SequenceStepId SequenceStepId, uint8_t SequenceStepEnabled)
{
	return (VL53L0X_Error)VL53L0X_SetSequenceStepEnable(impl->pDev, SequenceStepId, SequenceStepEnabled);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetSequenceStepEnable(VL53L0X_SequenceStepId SequenceStepId, uint8_t* pSequenceStepEnabled)
{
	return (VL53L0X_Error)VL53L0X_GetSequenceStepEnable(impl->pDev, SequenceStepId, pSequenceStepEnabled);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetSequenceStepEnables(VL53L0X_SchedulerSequenceSteps_t* pSchedulerSequenceSteps)
{
	return (VL53L0X_Error)VL53L0X_GetSequenceStepEnables(impl->pDev, (::VL53L0X_SchedulerSequenceSteps_t*) pSchedulerSequenceSteps);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetSequenceStepTimeout(VL53L0X_SequenceStepId SequenceStepId, FixPoint1616_t TimeOutMilliSecs)
{
	return (VL53L0X_Error)VL53L0X_SetSequenceStepTimeout(impl->pDev, SequenceStepId, TimeOutMilliSecs);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetSequenceStepTimeout(VL53L0X_SequenceStepId SequenceStepId, FixPoint1616_t* pTimeOutMilliSecs)
{
	return (VL53L0X_Error)VL53L0X_GetSequenceStepTimeout(impl->pDev, SequenceStepId, pTimeOutMilliSecs);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetNumberOfSequenceSteps(uint8_t* pNumberOfSequenceSteps)
{
	return (VL53L0X_Error)VL53L0X_GetNumberOfSequenceSteps(impl->pDev, pNumberOfSequenceSteps);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetSequenceStepsInfo(VL53L0X_SequenceStepId SequenceStepId, char* pSequenceStepsString)
{
	return (VL53L0X_Error)VL53L0X_GetSequenceStepsInfo(SequenceStepId, pSequenceStepsString);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetInterMeasurementPeriodMilliSeconds(uint32_t InterMeasurementPeriodMilliSeconds)
{
	return (VL53L0X_Error)VL53L0X_SetInterMeasurementPeriodMilliSeconds(impl->pDev, InterMeasurementPeriodMilliSeconds);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetInterMeasurementPeriodMilliSeconds(uint32_t* pInterMeasurementPeriodMilliSeconds)
{
	return (VL53L0X_Error)VL53L0X_GetInterMeasurementPeriodMilliSeconds(impl->pDev, pInterMeasurementPeriodMilliSeconds);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetXTalkCompensationEnable(uint8_t XTalkCompensationEnable)
{
	return (VL53L0X_Error)VL53L0X_SetXTalkCompensationEnable(impl->pDev, XTalkCompensationEnable);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetXTalkCompensationEnable(uint8_t* pXTalkCompensationEnable)
{
	return (VL53L0X_Error)VL53L0X_GetXTalkCompensationEnable(impl->pDev, pXTalkCompensationEnable);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetXTalkCompensationRateMegaCps(FixPoint1616_t XTalkCompensationRateMegaCps)
{
	return (VL53L0X_Error)VL53L0X_SetXTalkCompensationRateMegaCps(impl->pDev, XTalkCompensationRateMegaCps);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetXTalkCompensationRateMegaCps(FixPoint1616_t* pXTalkCompensationRateMegaCps)
{
	return (VL53L0X_Error)VL53L0X_GetXTalkCompensationRateMegaCps(impl->pDev, pXTalkCompensationRateMegaCps);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetRefCalibration(uint8_t VhvSettings, uint8_t PhaseCal)
{
	return (VL53L0X_Error)VL53L0X_SetRefCalibration(impl->pDev, VhvSettings, PhaseCal);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetRefCalibration(uint8_t* pVhvSettings, uint8_t* pPhaseCal)
{
	return (VL53L0X_Error)VL53L0X_GetRefCalibration(impl->pDev, pVhvSettings, pPhaseCal);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetNumberOfLimitCheck(uint16_t* pNumberOfLimitCheck)
{
	return (VL53L0X_Error)VL53L0X_GetNumberOfLimitCheck(pNumberOfLimitCheck);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetLimitCheckInfo(uint16_t LimitCheckId, char* pLimitCheckString)
{
	return (VL53L0X_Error)VL53L0X_GetLimitCheckInfo(impl->pDev, LimitCheckId, pLimitCheckString);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetLimitCheckStatus(uint16_t LimitCheckId, uint8_t* pLimitCheckStatus)
{
	return (VL53L0X_Error)VL53L0X_GetLimitCheckStatus(impl->pDev, LimitCheckId, pLimitCheckStatus);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetLimitCheckEnable(uint16_t LimitCheckId, uint8_t LimitCheckEnable)
{
	return (VL53L0X_Error)VL53L0X_SetLimitCheckEnable(impl->pDev, LimitCheckId, LimitCheckEnable);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetLimitCheckEnable(uint16_t LimitCheckId, uint8_t* pLimitCheckEnable)
{
	return (VL53L0X_Error)VL53L0X_GetLimitCheckEnable(impl->pDev, LimitCheckId, pLimitCheckEnable);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetLimitCheckValue(uint16_t LimitCheckId, FixPoint1616_t LimitCheckValue)
{
	return (VL53L0X_Error)VL53L0X_SetLimitCheckValue(impl->pDev, LimitCheckId, LimitCheckValue);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetLimitCheckValue(uint16_t LimitCheckId, FixPoint1616_t* pLimitCheckValue)
{
	return (VL53L0X_Error)VL53L0X_GetLimitCheckValue(impl->pDev, LimitCheckId, pLimitCheckValue);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetLimitCheckCurrent(uint16_t LimitCheckId, FixPoint1616_t* pLimitCheckCurrent)
{
	return (VL53L0X_Error)VL53L0X_GetLimitCheckCurrent(impl->pDev, LimitCheckId, pLimitCheckCurrent);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetWrapAroundCheckEnable(uint8_t WrapAroundCheckEnable)
{
	return (VL53L0X_Error)VL53L0X_SetWrapAroundCheckEnable(impl->pDev, WrapAroundCheckEnable);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetWrapAroundCheckEnable(uint8_t* pWrapAroundCheckEnable)
{
	return (VL53L0X_Error)VL53L0X_GetWrapAroundCheckEnable(impl->pDev, pWrapAroundCheckEnable);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetDmaxCalParameters(uint16_t RangeMilliMeter, FixPoint1616_t SignalRateRtnMegaCps)
{
	return (VL53L0X_Error)VL53L0X_SetDmaxCalParameters(impl->pDev, RangeMilliMeter, SignalRateRtnMegaCps);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetDmaxCalParameters(uint16_t* pRangeMilliMeter, FixPoint1616_t* pSignalRateRtnMegaCps)
{
	return (VL53L0X_Error)VL53L0X_GetDmaxCalParameters(impl->pDev, pRangeMilliMeter, pSignalRateRtnMegaCps);
}

::VL53L0X::VL53L0X_Error VL53L0X::PerformSingleMeasurement()
{
	return (VL53L0X_Error)VL53L0X_PerformSingleMeasurement(impl->pDev);
}

::VL53L0X::VL53L0X_Error VL53L0X::PerformRefCalibration(uint8_t* pVhvSettings, uint8_t* pPhaseCal)
{
	return (VL53L0X_Error)VL53L0X_PerformRefCalibration(impl->pDev, pVhvSettings, pPhaseCal);
}

::VL53L0X::VL53L0X_Error VL53L0X::PerformXTalkMeasurement(uint32_t TimeoutMs, FixPoint1616_t* pXtalkPerSpad, uint8_t* pAmbientTooHigh)
{
	return (VL53L0X_Error)VL53L0X_PerformXTalkMeasurement(impl->pDev, TimeoutMs, pXtalkPerSpad, pAmbientTooHigh);
}

::VL53L0X::VL53L0X_Error VL53L0X::PerformXTalkCalibration(FixPoint1616_t XTalkCalDistance, FixPoint1616_t* pXTalkCompensationRateMegaCps)
{
	return (VL53L0X_Error)VL53L0X_PerformXTalkCalibration(impl->pDev, XTalkCalDistance, pXTalkCompensationRateMegaCps);
}

::VL53L0X::VL53L0X_Error VL53L0X::PerformOffsetCalibration(FixPoint1616_t CalDistanceMilliMeter, int32_t* pOffsetMicroMeter)
{
	return (VL53L0X_Error)VL53L0X_PerformOffsetCalibration(impl->pDev, (::FixPoint1616_t) CalDistanceMilliMeter,
	                                                        pOffsetMicroMeter);
}

::VL53L0X::VL53L0X_Error VL53L0X::StartMeasurement()
{
	return (VL53L0X_Error)VL53L0X_StartMeasurement(impl->pDev);
}

::VL53L0X::VL53L0X_Error VL53L0X::StopMeasurement()
{
	return (VL53L0X_Error)VL53L0X_StopMeasurement(impl->pDev);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetMeasurementDataReady(uint8_t* pMeasurementDataReady)
{
	return (VL53L0X_Error)VL53L0X_GetMeasurementDataReady(impl->pDev, pMeasurementDataReady);
}

::VL53L0X::VL53L0X_Error VL53L0X::WaitDeviceReadyForNewMeasurement(uint32_t MaxLoop)
{
	return (VL53L0X_Error)VL53L0X_WaitDeviceReadyForNewMeasurement(impl->pDev, MaxLoop);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetMeasurementRefSignal(FixPoint1616_t* pMeasurementRefSignal)
{
	return (VL53L0X_Error)VL53L0X_GetMeasurementRefSignal(impl->pDev, pMeasurementRefSignal);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetRangingMeasurementData(VL53L0X_RangingMeasurementData_t* pRangingMeasurementData)
{
	return (VL53L0X_Error)VL53L0X_GetRangingMeasurementData(impl->pDev, (::VL53L0X_RangingMeasurementData_t*) pRangingMeasurementData);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetHistogramMeasurementData(VL53L0X_HistogramMeasurementData_t* pHistogramMeasurementData)
{
	return (VL53L0X_Error)VL53L0X_GetHistogramMeasurementData(impl->pDev, (::VL53L0X_HistogramMeasurementData_t*) pHistogramMeasurementData);
}

::VL53L0X::VL53L0X_Error VL53L0X::PerformSingleRangingMeasurement(VL53L0X_RangingMeasurementData_t* pRangingMeasurementData)
{
	return (VL53L0X_Error)VL53L0X_PerformSingleRangingMeasurement(impl->pDev, (::VL53L0X_RangingMeasurementData_t*) pRangingMeasurementData);
}

::VL53L0X::VL53L0X_Error VL53L0X::PerformSingleHistogramMeasurement(VL53L0X_HistogramMeasurementData_t* pHistogramMeasurementData)
{
	return (VL53L0X_Error)VL53L0X_PerformSingleHistogramMeasurement(impl->pDev, (::VL53L0X_HistogramMeasurementData_t*) pHistogramMeasurementData);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetNumberOfROIZones(uint8_t NumberOfROIZones)
{
	return (VL53L0X_Error)VL53L0X_SetNumberOfROIZones(impl->pDev, NumberOfROIZones);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetNumberOfROIZones(uint8_t* pNumberOfROIZones)
{
	return (VL53L0X_Error)VL53L0X_GetNumberOfROIZones(impl->pDev, pNumberOfROIZones);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetMaxNumberOfROIZones(uint8_t* pMaxNumberOfROIZones)
{
	return (VL53L0X_Error)VL53L0X_GetMaxNumberOfROIZones(impl->pDev, pMaxNumberOfROIZones);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetGpioConfig(uint8_t Pin, VL53L0X_DeviceModes DeviceMode, VL53L0X_GpioFunctionality Functionality, VL53L0X_InterruptPolarity Polarity)
{
	return (VL53L0X_Error)VL53L0X_SetGpioConfig(impl->pDev, Pin, DeviceMode, Functionality, Polarity);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetGpioConfig(uint8_t Pin, VL53L0X_DeviceModes* pDeviceMode, VL53L0X_GpioFunctionality* pFunctionality, VL53L0X_InterruptPolarity* pPolarity)
{
	return (VL53L0X_Error)VL53L0X_GetGpioConfig(impl->pDev, Pin, (::VL53L0X_DeviceModes*) pDeviceMode, (::VL53L0X_GpioFunctionality*) pFunctionality, (::VL53L0X_InterruptPolarity*) pPolarity);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetInterruptThresholds(VL53L0X_DeviceModes DeviceMode, FixPoint1616_t ThresholdLow, FixPoint1616_t ThresholdHigh)
{
	return (VL53L0X_Error)VL53L0X_SetInterruptThresholds(impl->pDev, DeviceMode, ThresholdLow, ThresholdHigh);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetInterruptThresholds(VL53L0X_DeviceModes DeviceMode, FixPoint1616_t* pThresholdLow, FixPoint1616_t* pThresholdHigh)
{
	return (VL53L0X_Error)VL53L0X_GetInterruptThresholds(impl->pDev, DeviceMode, pThresholdLow, pThresholdHigh);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetStopCompletedStatus(uint32_t* pStopStatus)
{
	return (VL53L0X_Error)VL53L0X_GetStopCompletedStatus(impl->pDev, pStopStatus);
}

::VL53L0X::VL53L0X_Error VL53L0X::ClearInterruptMask(uint32_t InterruptMask)
{
	return (VL53L0X_Error)VL53L0X_ClearInterruptMask(impl->pDev, InterruptMask);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetInterruptMaskStatus(uint32_t* pInterruptMaskStatus)
{
	return (VL53L0X_Error)VL53L0X_GetInterruptMaskStatus(impl->pDev, pInterruptMaskStatus);
}

::VL53L0X::VL53L0X_Error VL53L0X::EnableInterruptMask(uint32_t InterruptMask)
{
	return (VL53L0X_Error)VL53L0X_EnableInterruptMask(impl->pDev, InterruptMask);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetSpadAmbientDamperThreshold(uint16_t SpadAmbientDamperThreshold)
{
	return (VL53L0X_Error)VL53L0X_SetSpadAmbientDamperThreshold(impl->pDev, SpadAmbientDamperThreshold);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetSpadAmbientDamperThreshold(uint16_t* pSpadAmbientDamperThreshold)
{
	return (VL53L0X_Error)VL53L0X_GetSpadAmbientDamperThreshold(impl->pDev, pSpadAmbientDamperThreshold);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetSpadAmbientDamperFactor(uint16_t SpadAmbientDamperFactor)
{
	return (VL53L0X_Error)VL53L0X_SetSpadAmbientDamperFactor(impl->pDev, SpadAmbientDamperFactor);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetSpadAmbientDamperFactor(uint16_t* pSpadAmbientDamperFactor)
{
	return (VL53L0X_Error)VL53L0X_GetSpadAmbientDamperFactor(impl->pDev, pSpadAmbientDamperFactor);
}

::VL53L0X::VL53L0X_Error VL53L0X::PerformRefSpadManagement(uint32_t* refSpadCount, uint8_t* isApertureSpads)
{
	return (VL53L0X_Error)VL53L0X_PerformRefSpadManagement(impl->pDev, refSpadCount, isApertureSpads);
}

::VL53L0X::VL53L0X_Error VL53L0X::SetReferenceSpads(uint32_t refSpadCount, uint8_t isApertureSpads)
{
	return (VL53L0X_Error)VL53L0X_SetReferenceSpads(impl->pDev, refSpadCount, isApertureSpads);
}

::VL53L0X::VL53L0X_Error VL53L0X::GetReferenceSpads(uint32_t* refSpadCount, uint8_t* isApertureSpads)
{
	return (VL53L0X_Error)VL53L0X_GetReferenceSpads(impl->pDev, refSpadCount, isApertureSpads);
}

}
