#include "VL53L0X_Platform.hpp"
#include "vl53l0x_platform.h"

using VL53L0X::VL53L0X_Platform;

extern "C" {

VL53L0X_Error VL53L0X_LockSequenceAccess(VL53L0X_DEV Dev)
{
	return ((VL53L0X_Platform*)Dev->ImplData)->LockSequenceAccess();
}

VL53L0X_Error VL53L0X_UnlockSequenceAccess(VL53L0X_DEV Dev)
{
	return ((VL53L0X_Platform*)Dev->ImplData)->UnlockSequenceAccess();
}


VL53L0X_Error VL53L0X_WriteMulti(VL53L0X_DEV Dev, uint8_t index, uint8_t* pdata, uint32_t count)
{
	return ((VL53L0X_Platform*)Dev->ImplData)->WriteMulti(index, pdata, count);
}

VL53L0X_Error VL53L0X_ReadMulti(VL53L0X_DEV Dev, uint8_t index, uint8_t* pdata, uint32_t count)
{
	return ((VL53L0X_Platform*)Dev->ImplData)->ReadMulti(index, pdata, count);
}

VL53L0X_Error VL53L0X_WrByte(VL53L0X_DEV Dev, uint8_t index, uint8_t data)
{
	return ((VL53L0X_Platform*)Dev->ImplData)->WrByte(index, data);
}

VL53L0X_Error VL53L0X_WrWord(VL53L0X_DEV Dev, uint8_t index, uint16_t data)
{
	return ((VL53L0X_Platform*)Dev->ImplData)->WrWord(index, data);
}

VL53L0X_Error VL53L0X_WrDWord(VL53L0X_DEV Dev, uint8_t index, uint32_t data)
{
	return ((VL53L0X_Platform*)Dev->ImplData)->WrDWord(index, data);
}

VL53L0X_Error VL53L0X_RdByte(VL53L0X_DEV Dev, uint8_t index, uint8_t* data)
{
	return ((VL53L0X_Platform*)Dev->ImplData)->RdByte(index, data);
}

VL53L0X_Error VL53L0X_RdWord(VL53L0X_DEV Dev, uint8_t index, uint16_t* data)
{
	return ((VL53L0X_Platform*)Dev->ImplData)->RdWord(index, data);
}

VL53L0X_Error VL53L0X_RdDWord(VL53L0X_DEV Dev, uint8_t index, uint32_t* data)
{
	return ((VL53L0X_Platform*)Dev->ImplData)->RdDWord(index, data);
}

VL53L0X_Error VL53L0X_UpdateByte(VL53L0X_DEV Dev, uint8_t index, uint8_t AndData, uint8_t OrData)
{
	return ((VL53L0X_Platform*)Dev->ImplData)->UpdateByte(index, AndData, OrData);
}


VL53L0X_Error VL53L0X_PollingDelay(VL53L0X_DEV Dev)
{
	return ((VL53L0X_Platform*)Dev->ImplData)->PollingDelay();
}

}
