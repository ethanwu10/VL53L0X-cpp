#ifndef _VL53L0X_PLATFORM_HPP
#define _VL53L0X_PLATFORM_HPP

#include "VL53L0X_Types.hpp"

namespace VL53L0X
{

class VL53L0X;

class VL53L0X_Platform
{
public:

	/**
	 * Lock comms interface to serialize all commands to a shared I2C interface for a specific device
	 * @param   Dev       Device Handle
	 * @return  VL53L0X_ERROR_NONE        Success
	 * @return  "Other error code"    See ::VL53L0X_Error
	 */
	virtual VL53L0X_Error LockSequenceAccess() { return VL53L0X_ERROR_NOT_IMPLEMENTED; }

	/**
	 * Unlock comms interface to serialize all commands to a shared I2C interface for a specific device
	 * @param   Dev       Device Handle
	 * @return  VL53L0X_ERROR_NONE        Success
	 * @return  "Other error code"    See ::VL53L0X_Error
	 */
	virtual VL53L0X_Error UnlockSequenceAccess() { return VL53L0X_ERROR_NOT_IMPLEMENTED; }


	/**
	 * Writes the supplied byte buffer to the device
	 * @param   Dev       Device Handle
	 * @param   index     The register index
	 * @param   pdata     Pointer to uint8_t buffer containing the data to be written
	 * @param   count     Number of bytes in the supplied byte buffer
	 * @return  VL53L0X_ERROR_NONE        Success
	 * @return  "Other error code"    See ::VL53L0X_Error
	 */
	virtual VL53L0X_Error
	WriteMulti(uint8_t index, uint8_t* pdata, uint32_t count) { return VL53L0X_ERROR_NOT_IMPLEMENTED; }

	/**
	 * Reads the requested number of bytes from the device
	 * @param   Dev       Device Handle
	 * @param   index     The register index
	 * @param   pdata     Pointer to the uint8_t buffer to store read data
	 * @param   count     Number of uint8_t's to read
	 * @return  VL53L0X_ERROR_NONE        Success
	 * @return  "Other error code"    See ::VL53L0X_Error
	 */
	virtual VL53L0X_Error
	ReadMulti(uint8_t index, uint8_t* pdata, uint32_t count) { return VL53L0X_ERROR_NOT_IMPLEMENTED; }

	/**
	 * Write single byte register
	 * @param   Dev       Device Handle
	 * @param   index     The register index
	 * @param   data      8 bit register data
	 * @return  VL53L0X_ERROR_NONE        Success
	 * @return  "Other error code"    See ::VL53L0X_Error
	 */
	virtual VL53L0X_Error
	WrByte(uint8_t index, uint8_t data) { return VL53L0X_ERROR_NOT_IMPLEMENTED; }

	/**
	 * Write word register
	 * @param   Dev       Device Handle
	 * @param   index     The register index
	 * @param   data      16 bit register data
	 * @return  VL53L0X_ERROR_NONE        Success
	 * @return  "Other error code"    See ::VL53L0X_Error
	 */
	virtual VL53L0X_Error
	WrWord(uint8_t index, uint16_t data) { return VL53L0X_ERROR_NOT_IMPLEMENTED; }

	/**
	 * Write double word (4 byte) register
	 * @param   Dev       Device Handle
	 * @param   index     The register index
	 * @param   data      32 bit register data
	 * @return  VL53L0X_ERROR_NONE        Success
	 * @return  "Other error code"    See ::VL53L0X_Error
	 */
	virtual VL53L0X_Error
	WrDWord(uint8_t index, uint32_t data) { return VL53L0X_ERROR_NOT_IMPLEMENTED; }

	/**
	 * Read single byte register
	 * @param   Dev       Device Handle
	 * @param   index     The register index
	 * @param   data      pointer to 8 bit data
	 * @return  VL53L0X_ERROR_NONE        Success
	 * @return  "Other error code"    See ::VL53L0X_Error
	 */
	virtual VL53L0X_Error
	RdByte(uint8_t index, uint8_t* data) { return VL53L0X_ERROR_NOT_IMPLEMENTED; }

	/**
	 * Read word (2byte) register
	 * @param   Dev       Device Handle
	 * @param   index     The register index
	 * @param   data      pointer to 16 bit data
	 * @return  VL53L0X_ERROR_NONE        Success
	 * @return  "Other error code"    See ::VL53L0X_Error
	 */
	virtual VL53L0X_Error
	RdWord(uint8_t index, uint16_t* data) { return VL53L0X_ERROR_NOT_IMPLEMENTED; }

	/**
	 * Read dword (4byte) register
	 * @param   Dev       Device Handle
	 * @param   index     The register index
	 * @param   data      pointer to 32 bit data
	 * @return  VL53L0X_ERROR_NONE        Success
	 * @return  "Other error code"    See ::VL53L0X_Error
	 */
	virtual VL53L0X_Error
	RdDWord(uint8_t index, uint32_t* data) { return VL53L0X_ERROR_NOT_IMPLEMENTED; }

	/**
	 * Threat safe Update (read/modify/write) single byte register
	 *
	 * Final_reg = (Initial_reg & and_data) |or_data
	 *
	 * @param   Dev        Device Handle
	 * @param   index      The register index
	 * @param   AndData    8 bit and data
	 * @param   OrData     8 bit or data
	 * @return  VL53L0X_ERROR_NONE        Success
	 * @return  "Other error code"    See ::VL53L0X_Error
	 */
	virtual VL53L0X_Error
	UpdateByte(uint8_t index, uint8_t AndData, uint8_t OrData);


	/**
	 * @brief execute delay in all polling API call
	 *
	 * A typical multi-thread or RTOs implementation is to sleep the task for some 5ms (with 100Hz max rate faster polling is not needed)
	 * if nothing specific is need you can define it as an empty/void macro
	 * @code
	 * #define VL53L0X_PollingDelay(...) (void)0
	 * @endcode
	 * @param Dev       Device Handle
	 * @return  VL53L0X_ERROR_NONE        Success
	 * @return  "Other error code"    See ::VL53L0X_Error
	 */
	virtual VL53L0X_Error PollingDelay() { return VL53L0X_ERROR_NOT_IMPLEMENTED; }


	/**
	 * Update the stored address with the new, changed address
	 *
	 * @param DeviceAddress New address of device
	 */
	virtual void UpdateAddress(uint8_t DeviceAddress) {}
};

} // namespace VL53L0X

#endif //_VL53L0X_PLATFORM_HPP
