#ifndef _VL53L0X_TYPES_HPP
#define _VL53L0X_TYPES_HPP

#include <cstdint>

namespace VL53L0X
{

/**
 * use where fractional values are expected
 *
 * Given a floating point value f its .16 bit point is (int)(f*(1<<16))*/
typedef uint32_t FixPoint1616_t;

static const uint8_t VL53L0X_MAX_STRING_LENGTH = 32;

/**
 * @brief Defines the parameters of the Get Device Info Functions
 */
struct VL53L0X_DeviceInfo_t
{
	char Name[VL53L0X_MAX_STRING_LENGTH];
	/*!< Name of the Device e.g. Left_Distance */
	char Type[VL53L0X_MAX_STRING_LENGTH];
	/*!< Type of the Device e.g VL53L0X */
	char ProductId[VL53L0X_MAX_STRING_LENGTH];
	/*!< Product Identifier String	*/
	uint8_t ProductType;
	/*!< Product Type, VL53L0X = 1, VL53L1 = 2 */
	uint8_t ProductRevisionMajor;
	/*!< Product revision major */
	uint8_t ProductRevisionMinor;
	/*!< Product revision minor */
};

/**
 * @brief Error and Warning code returned by API
 */
enum VL53L0X_Error : int8_t
{
	VL53L0X_ERROR_NONE = 0,
	VL53L0X_ERROR_CALIBRATION_WARNING = -1, ///< Warning invalid calibration data may be in use
	VL53L0X_ERROR_MIN_CLIPPED = -2, ///< Warning parameter passed was clipped to min before to be applied
	VL53L0X_ERROR_UNDEFINED = -3, ///< Unqualified error
	VL53L0X_ERROR_INVALID_PARAMS = -4, ///< Parameter passed is invalid or out of range
	VL53L0X_ERROR_NOT_SUPPORTED = -5, ///< Function is not supported in current mode or configuration
	VL53L0X_ERROR_RANGE_ERROR = -6, ///< Device report a ranging error interrupt status
	VL53L0X_ERROR_TIME_OUT = -7, ///< Aborted due to time out
	VL53L0X_ERROR_MODE_NOT_SUPPORTED = -8, ///< Asked mode is not supported by the device
	VL53L0X_ERROR_BUFFER_TOO_SMALL = -9, ///< ...
	VL53L0X_ERROR_GPIO_NOT_EXISTING = -10, ///< User tried to setup a non-existing GPIO pin
	VL53L0X_ERROR_GPIO_FUNCTIONALITY_NOT_SUPPORTED = -11, ///< unsupported GPIO functionality
	VL53L0X_ERROR_INTERRUPT_NOT_CLEARED = -12, ///< Error during interrupt clear
	VL53L0X_ERROR_CONTROL_INTERFACE = -20, ///< error reported from IO functions
	VL53L0X_ERROR_INVALID_COMMAND = -30, ///< The command is not allowed in the current device state (power down)
	VL53L0X_ERROR_DIVISION_BY_ZERO = -40, ///< In the function a division by zero occurs
	VL53L0X_ERROR_REF_SPAD_INIT = -50,///< Error during reference SPAD initialization
	VL53L0X_ERROR_NOT_IMPLEMENTED = -99 ///< Tells requested functionality has not been implemented yet or
	///< not compatible with the device
};

/**
 * @brief Defines Device modes
 * Defines all possible modes for the device
 */
enum VL53L0X_DeviceModes : uint8_t
{
	VL53L0X_DEVICEMODE_SINGLE_RANGING = 0,
	VL53L0X_DEVICEMODE_CONTINUOUS_RANGING = 1,
	VL53L0X_DEVICEMODE_SINGLE_HISTOGRAM = 2,
	VL53L0X_DEVICEMODE_CONTINUOUS_TIMED_RANGING = 3,
	VL53L0X_DEVICEMODE_SINGLE_ALS = 10,
	VL53L0X_DEVICEMODE_GPIO_DRIVE = 20,
	VL53L0X_DEVICEMODE_GPIO_OSC = 21
};

/**
 * @brief Defines Histogram modes
 *	Defines all possible Histogram modes for the device
 */
enum VL53L0X_HistogramModes : uint8_t
{
	VL53L0X_HISTOGRAMMODE_DISABLED = 0, ///< Histogram Disabled
	VL53L0X_HISTOGRAMMODE_REFERENCE_ONLY = 1, ///< Histogram Reference array only
	VL53L0X_HISTOGRAMMODE_RETURN_ONLY = 2, ///< Histogram Return array only
	VL53L0X_HISTOGRAMMODE_BOTH = 3 ///< Histogram both Reference and Return Arrays
};

/**
 * @brief List of available Power Modes
 *	List of available Power Modes
 */
enum VL53L0X_PowerModes : uint8_t
{
	VL53L0X_POWERMODE_STANDBY_LEVEL1 = 0, ///< Standby level 1
	VL53L0X_POWERMODE_STANDBY_LEVEL2 = 1, ///< Standby level 2
	VL53L0X_POWERMODE_IDLE_LEVEL1 = 2, ///< Idle level 1
	VL53L0X_POWERMODE_IDLE_LEVEL2 = 3 ///< Idle level 2
};

static const uint8_t VL53L0X_CHECKENABLE_NUMBER_OF_CHECKS = 6;
/**
 * @brief Defines all parameters for the device
 */
struct VL53L0X_DeviceParameters_t
{
	VL53L0X_DeviceModes DeviceMode;
	/*!< Defines type of measurement to be done for the next measure */
	VL53L0X_HistogramModes HistogramMode;

	/*!< Defines type of histogram measurement to be done for the next
	 *	measure */
	uint32_t MeasurementTimingBudgetMicroSeconds;
	/*!< Defines the allowed total time for a single measurement */
	uint32_t InterMeasurementPeriodMilliSeconds;

	/*!< Defines time between two consecutive measurements (between two
	 *	measurement starts). If set to 0 means back-to-back mode */
	uint8_t XTalkCompensationEnable;
	/*!< Tells if Crosstalk compensation shall be enable or not	 */
	uint16_t XTalkCompensationRangeMilliMeter;
	/*!< CrossTalk compensation range in millimeter	 */
	FixPoint1616_t XTalkCompensationRateMegaCps;

	/*!< CrossTalk compensation rate in Mega counts per seconds.
	 *	Expressed in 16.16 fixed point format.	*/
	int32_t RangeOffsetMicroMeters;
	/*!< Range offset adjustment (mm).	*/

	uint8_t LimitChecksEnable[VL53L0X_CHECKENABLE_NUMBER_OF_CHECKS];
	/*!< This Array store all the Limit Check enable for this device. */
	uint8_t LimitChecksStatus[VL53L0X_CHECKENABLE_NUMBER_OF_CHECKS];

	/*!< This Array store all the Status of the check linked to last
	 * measurement. */
	FixPoint1616_t LimitChecksValue[VL53L0X_CHECKENABLE_NUMBER_OF_CHECKS];
	/*!< This Array store all the Limit Check value for this device */

	uint8_t WrapAroundCheckEnable;
	/*!< Tells if Wrap Around Check shall be enable or not */
};

/**
 * @brief Defines the current status of the device
 *	Defines the current status of the device
 */
enum VL53L0X_State : uint8_t
{
	VL53L0X_STATE_POWERDOWN = 0, ///< Device is in HW reset
	VL53L0X_STATE_WAIT_STATICINIT = 1, ///< Device is initialized and wait for static initialization
	VL53L0X_STATE_STANDBY = 2, ///< Device is in Low power Standby mode
	VL53L0X_STATE_IDLE = 3, ///< Device has been initialized and ready to do measurements
	VL53L0X_STATE_RUNNING = 4, ///< Device is performing measurement
	VL53L0X_STATE_UNKNOWN = 98, ///< Device is in unknown state and need to be rebooted
	VL53L0X_STATE_ERROR = 99, ///< Device is in error state and need to be rebooted
};

/**
 * @brief Structure containing the Dmax computation parameters and data
 */
struct VL53L0X_DMaxData_t
{
	int32_t AmbTuningWindowFactor_K;
	/*!<  internal algo tuning (*1000) */
	int32_t RetSignalAt0mm;
	/*!< intermediate dmax computation value caching */
};

/**
 * @struct VL53L0X_RangeData_t
 * @brief Range measurement data.
 */
struct VL53L0X_RangingMeasurementData_t
{
	uint32_t TimeStamp;     /*!< 32-bit time stamp. */
	uint32_t MeasurementTimeUsec;

	/*!< Give the Measurement time needed by the device to do the
	 * measurement.*/


	uint16_t RangeMilliMeter;   /*!< range distance in millimeter. */

	uint16_t RangeDMaxMilliMeter;

	/*!< Tells what is the maximum detection distance of the device
	 * in current setup and environment conditions (Filled when
	 *	applicable) */

	FixPoint1616_t SignalRateRtnMegaCps;

	/*!< Return signal rate (MCPS)\n these is a 16.16 fix point
	 *	value, which is effectively a measure of target
	 *	 reflectance.*/
	FixPoint1616_t AmbientRateRtnMegaCps;

	/*!< Return ambient rate (MCPS)\n these is a 16.16 fix point
	 *	value, which is effectively a measure of the ambien
	 *	t light.*/

	uint16_t EffectiveSpadRtnCount;

	/*!< Return the effective SPAD count for the return signal.
	 *	To obtain Real value it should be divided by 256 */

	uint8_t ZoneId;

	/*!< Denotes which zone and range scheduler stage the range
	 *	data relates to. */
	uint8_t RangeFractionalPart;

	/*!< Fractional part of range distance. Final value is a
	 *	FixPoint168 value. */
	uint8_t RangeStatus;

	/*!< Range Status for the current measurement. This is device
	 *	dependent. Value = 0 means value is valid.
	 *	See \ref RangeStatusPage */
};

/**
 *  @brief Device Error code
 *
 *  This enum is Device specific it should be updated in the implementation
 *  Use @a VL53L0X_GetStatusErrorString() to get the string.
 *  It is related to Status Register of the Device.
 */
enum VL53L0X_DeviceError : uint8_t
{
	VL53L0X_DEVICEERROR_NONE = 0,
	VL53L0X_DEVICEERROR_VCSELCONTINUITYTESTFAILURE = 1,
	VL53L0X_DEVICEERROR_VCSELWATCHDOGTESTFAILURE = 2,
	VL53L0X_DEVICEERROR_NOVHVVALUEFOUND = 3,
	VL53L0X_DEVICEERROR_MSRCNOTARGET = 4,
	VL53L0X_DEVICEERROR_SNRCHECK = 5,
	VL53L0X_DEVICEERROR_RANGEPHASECHECK = 6,
	VL53L0X_DEVICEERROR_SIGMATHRESHOLDCHECK = 7,
	VL53L0X_DEVICEERROR_TCC = 8,
	VL53L0X_DEVICEERROR_PHASECONSISTENCY = 9,
	VL53L0X_DEVICEERROR_MINCLIP = 10,
	VL53L0X_DEVICEERROR_RANGECOMPLETE = 11,
	VL53L0X_DEVICEERROR_ALGOUNDERFLOW = 12,
	VL53L0X_DEVICEERROR_ALGOOVERFLOW = 13,
	VL53L0X_DEVICEERROR_RANGEIGNORETHRESHOLD = 14,
};

static const uint8_t VL53L0X_HISTOGRAM_BUFFER_SIZE = 24;

/**
 * @struct VL53L0X_HistogramData_t
 * @brief Histogram measurement data.
 */
struct VL53L0X_HistogramMeasurementData_t
{
	/* Histogram Measurement data */
	uint32_t HistogramData[VL53L0X_HISTOGRAM_BUFFER_SIZE];
	/*!< Histogram data */
	uint8_t HistogramType; /*!< Indicate the types of histogram data :
	                        * Return only, Reference only, both Return and Reference */
	uint8_t FirstBin; /*!< First Bin value */
	uint8_t BufferSize; /*!< Buffer Size - Set by the user.*/
	uint8_t NumberOfBins;
	/*!< Number of bins filled by the histogram measurement */

	VL53L0X_DeviceError ErrorStatus;

	/*!< Error status of the current measurement. \n
	 * see @a ::VL53L0X_DeviceError @a VL53L0X_GetStatusErrorString() */
};

static const uint8_t VL53L0X_REF_SPAD_BUFFER_SIZE = 6;

/**
 * @struct VL53L0X_SpadData_t
 * @brief Spad Configuration Data.
 */
struct VL53L0X_SpadData_t
{
	uint8_t RefSpadEnables[VL53L0X_REF_SPAD_BUFFER_SIZE];
	/*!< Reference Spad Enables */
	uint8_t RefGoodSpadMap[VL53L0X_REF_SPAD_BUFFER_SIZE];
	/*!< Reference Spad Good Spad Map */
};

/**
 *  @brief Defines the different functionalities for the device GPIO(s)
 */
enum VL53L0X_GpioFunctionality : uint8_t
{
	VL53L0X_GPIOFUNCTIONALITY_OFF = 0,   ///< NO Interrupt
	VL53L0X_GPIOFUNCTIONALITY_THRESHOLD_CROSSED_LOW = 1,   ///< Level Low (value < thresh_low)
	VL53L0X_GPIOFUNCTIONALITY_THRESHOLD_CROSSED_HIGH = 2,   ///< Level High (value > thresh_high)
	VL53L0X_GPIOFUNCTIONALITY_THRESHOLD_CROSSED_OUT = 3,   ///< Out Of Window (value < thresh_low OR value > thresh_high)
	VL53L0X_GPIOFUNCTIONALITY_NEW_MEASURE_READY = 4,   ///< New Sample Ready
};

struct VL53L0X_DeviceSpecificParameters_t
{
	FixPoint1616_t OscFrequencyMHz; /* Frequency used */

	uint16_t LastEncodedTimeout;
	/* last encoded Time out used for timing budget*/

	VL53L0X_GpioFunctionality Pin0GpioFunctionality;
	/* store the functionality of the GPIO: pin0 */

	uint32_t FinalRangeTimeoutMicroSecs;
	/*!< Execution time of the final range*/
	uint8_t FinalRangeVcselPulsePeriod;
	/*!< Vcsel pulse period (pll clocks) for the final range measurement*/
	uint32_t PreRangeTimeoutMicroSecs;
	/*!< Execution time of the final range*/
	uint8_t PreRangeVcselPulsePeriod;
	/*!< Vcsel pulse period (pll clocks) for the pre-range measurement*/

	uint16_t SigmaEstRefArray;
	/*!< Reference array sigma value in 1/100th of [mm] e.g. 100 = 1mm */
	uint16_t SigmaEstEffPulseWidth;

	/*!< Effective Pulse width for sigma estimate in 1/100th
	 * of ns e.g. 900 = 9.0ns */
	uint16_t SigmaEstEffAmbWidth;

	/*!< Effective Ambient width for sigma estimate in 1/100th of ns
	 * e.g. 500 = 5.0ns */


	uint8_t ReadDataFromDeviceDone; /* Indicate if read from device has
	                                 * been done (==1) or not (==0) */
	uint8_t ModuleId; /* Module ID */
	uint8_t Revision; /* test Revision */
	char ProductId[VL53L0X_MAX_STRING_LENGTH];
	/* Product Identifier String  */
	uint8_t ReferenceSpadCount; /* used for ref spad management */
	uint8_t ReferenceSpadType;  /* used for ref spad management */
	uint8_t RefSpadsInitialised; /* reports if ref spads are initialised. */
	uint32_t PartUIDUpper; /*!< Unique Part ID Upper */
	uint32_t PartUIDLower; /*!< Unique Part ID Lower */
	FixPoint1616_t SignalRateMeasFixed400mm; /*!< Peek Signal rate
	                                          * at 400 mm*/

};

/**
 * @struct VL53L0X_DevData_t
 *
 * @brief VL53L0X PAL device ST private data structure \n
 * End user should never access any of these field directly
 *
 * These must never access directly but only via macro
 */
struct VL53L0X_DevData_t
{
	VL53L0X_DMaxData_t DMaxData;
	/*!< Dmax Data */
	int32_t Part2PartOffsetNVMMicroMeter;
	/*!< backed up NVM value */
	int32_t Part2PartOffsetAdjustmentNVMMicroMeter;
	/*!< backed up NVM value representing additional offset adjustment */
	VL53L0X_DeviceParameters_t CurrentParameters;
	/*!< Current Device Parameter */
	VL53L0X_RangingMeasurementData_t LastRangeMeasure;
	/*!< Ranging Data */
	VL53L0X_HistogramMeasurementData_t LastHistogramMeasure;
	/*!< Histogram Data */
	VL53L0X_DeviceSpecificParameters_t DeviceSpecificParameters;
	/*!< Parameters specific to the device */
	VL53L0X_SpadData_t SpadData;
	/*!< Spad Data */
	uint8_t SequenceConfig;
	/*!< Internal value for the sequence config */
	uint8_t RangeFractionalEnable;
	/*!< Enable/Disable fractional part of ranging data */
	VL53L0X_State PalState;
	/*!< Current state of the PAL for this device */
	VL53L0X_PowerModes PowerMode;
	/*!< Current Power Mode	 */
	uint16_t SigmaEstRefArray;
	/*!< Reference array sigma value in 1/100th of [mm] e.g. 100 = 1mm */
	uint16_t SigmaEstEffPulseWidth;

	/*!< Effective Pulse width for sigma estimate in 1/100th
	 * of ns e.g. 900 = 9.0ns */
	uint16_t SigmaEstEffAmbWidth;

	/*!< Effective Ambient width for sigma estimate in 1/100th of ns
	 * e.g. 500 = 5.0ns */
	uint8_t StopVariable;
	/*!< StopVariable used during the stop sequence */
	uint16_t targetRefRate;
	/*!< Target Ambient Rate for Ref spad management */
	FixPoint1616_t SigmaEstimate;

	/*!< Sigma Estimate - based on ambient & VCSEL rates and
	 * signal_total_events */
	FixPoint1616_t SignalEstimate;
	/*!< Signal Estimate - based on ambient & VCSEL rates and cross talk */
	FixPoint1616_t LastSignalRefMcps;
	/*!< Latest Signal ref in Mcps */
	uint8_t* pTuningSettingsPointer;
	/*!< Pointer for Tuning Settings table */
	uint8_t UseInternalTuningSettings;
	/*!< Indicate if we use	 Tuning Settings table */
	uint16_t LinearityCorrectiveGain;
	/*!< Linearity Corrective Gain value in x1000 */
	uint16_t DmaxCalRangeMilliMeter;
	/*!< Dmax Calibration Range millimeter */
	FixPoint1616_t DmaxCalSignalRateRtnMegaCps;
	/*!< Dmax Calibration Signal Rate Return MegaCps */

};

/**
 * @brief Defines the Polarity of the Interrupt
 */
enum VL53L0X_InterruptPolarity : uint8_t
{
	VL53L0X_INTERRUPTPOLARITY_LOW = 0, ///< Set active low polarity best setup for falling edge.
	VL53L0X_INTERRUPTPOLARITY_HIGH = 1, ///< Set active high polarity best setup for rising edge.
};

/**
 *	Defines the range measurement for which to access the VCSEL period.
 */
enum VL53L0X_VcselPeriod : uint8_t
{
	VL53L0X_VCSEL_PERIOD_PRE_RANGE = 0, ///< Identifies the pre-range vcsel period.
	VL53L0X_VCSEL_PERIOD_FINAL_RANGE = 1, ///< Identifies the final range vcsel period.
};

/**
 *	Defines the states of all the steps in the scheduler
 *	i.e. enabled/disabled.
 */
struct VL53L0X_SchedulerSequenceSteps_t
{
	uint8_t TccOn;         /*!<Reports if Target Centre Check On  */
	uint8_t MsrcOn;            /*!<Reports if MSRC On  */
	uint8_t DssOn;             /*!<Reports if DSS On  */
	uint8_t PreRangeOn;        /*!<Reports if Pre-Range On	*/
	uint8_t FinalRangeOn;      /*!<Reports if Final-Range On  */
};
/**
 * Defines the the sequence steps performed during ranging..
 */
enum VL53L0X_SequenceStepId : uint8_t
{
	VL53L0X_SEQUENCESTEP_TCC = 0, ///< Target CentreCheck identifier.
	VL53L0X_SEQUENCESTEP_DSS = 1, ///< Dynamic Spad Selection function Identifier.
	VL53L0X_SEQUENCESTEP_MSRC = 2, ///< Minimum Signal Rate Check function Identifier.
	VL53L0X_SEQUENCESTEP_PRE_RANGE = 3, ///< Pre-Range check Identifier.
	VL53L0X_SEQUENCESTEP_FINAL_RANGE = 4, ///< Final Range Check Identifier.
};

}

#endif //_VL53L0X_TYPES_HPP
